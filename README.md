# Arduino Time Interrupt

## Purpose

This simple sketch is designed to print the "runtime" of the Arduino to the
serial console, and pause timekeeping/printing when an interrupt is fired.  Time
will remain halted until a "GO" button is pressed, to continue timekeeping and
printing.

## Download

    cd ~/
    git clone https://bitbucket.org/mdill/arduino_time_interrupt.git

## License

This project is licensed under the BSD License - see the [LICENSE.md](https://bitbucket.org/mdill/arduino_time_interrupt/src/master/LICENSE.txt) file for
details.

