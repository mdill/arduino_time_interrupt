
// Define our pushbutton
#define buttonStop  2
#define buttonGo    3

// Track our global time variables
unsigned long startTime;

void setup(){
    // Set up our GO button
    pinMode( buttonGo, INPUT_PULLUP );
    
    // Begin serial comms for time output
    pinMode( buttonStop, INPUT_PULLUP );
    Serial.begin( 115200 );
    
    // Set up hardware interrupt
    attachInterrupt( digitalPinToInterrupt( buttonStop ), pauseMe, FALLING );
    
    startTime = millis();
}

void loop(){
    updateTime();
    updateSerial();
}

// Updade our time markers
void updateTime(){
    static unsigned long runTime;
    runTime = ( millis() - startTime ) / 1000;      // Current time minus the start time gives us how long we've been running in seconds

    s = ( runTime ) % 60;         // Runtime in seconds, modulo'ed with 60 seconds per minute, rounded down as an <int>
    m = ( runTime / 60 ) % 60;    // Runtime in seconds, divided by 60 s/min, modulo`ed with 60 gives us the minutes runtime, rounded down as an <int>
    h = runTime / (60 * 60);      // Runtime in seconds, divided by (60 min/hr * 60 s/min, rounded down as an <int>
}

// Update our LCD screen
void updateSerial(){    
    if( h < 10 )                        // Print hours running
        Serial.print( "0" );
    Serial.print( h );
    Serial.print( ":" );
    
    if( m < 10 )                        // Print minutes running
        Serial.print( "0" );
    Serial.print( m );
    Serial.print( ":" );
    
    if( s < 10 )                        // Print seconds running
        Serial.print( "0" );
    Serial.print( s );

    Serial.print( "\t" );
    Serial.print( millis() - startTime );
    Serial.print( "\n" );
}

void pauseMe(){
    Serial.print( "\n\nWe've been stopped!!!\n\n" );
    
    while( digitalRead( buttonGo ) == HIGH ){}
}
